from django.db import models
from django.utils import timezone

class cliente(models.Model):
    listaGenero = (
        ('f','femenino'),
        ('m','masculino'),

    )
    id_cliente = models.AutoField(primary_key=True)
    cedula = models.CharField(max_length=10)
    apellidos = models.CharField(max_length=25)
    nombres = models.CharField(max_length=25)
    genero = models.CharField(max_length=15,choices = listaGenero, null = True)
    banco = models.CharField(max_length= 10)
    estado = models.BooleanField(default=True)
    def guardar(self):
        self.save()


class banco(models.Model):
    cedula=models.CharField(max_length= 20)
    nombres=models.CharField(max_length= 60)
    apellidos=models.CharField(max_length= 60)
    numCuenta=models.IntegerField()
    cuenta=models.CharField(max_length= 40)

class cuenta(models.Model):
    tiposCuenta= (
        ('c','cuenta corriente'),
        ('a','cuenta de ahorro'),
        ('n','cuenta nomina'),
        ('v','cuenta de valores'),
        ('e','cuenta para empresas y negocios')
    )
    id_cuenta= models.AutoField(primary_key=True)
    numero_de_cuenta = models.CharField(max_length=30)
    nombre = models.CharField(max_length=40)
    saldo = models.DecimalField(max_digits=10,decimal_places = 2,null = True)
    tipo_de_cuenta = models.CharField(max_length=30,choices= tiposCuenta,null=True)

class transaccion(models.Model):
    cuenta = models.ForeignKey(cuenta,on_delete= models.CASCADE)
    cliente = models.ForeignKey(cliente,on_delete= models.CASCADE)
    monto = models.DecimalField(max_digits=10,decimal_places = 2)
    fecha = models.DateField(auto_now = False,auto_now_add=False)
