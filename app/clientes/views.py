from django.shortcuts import render,redirect
from django.http import HttpResponse
from .forms import FormularioCliente
from app.modelo.models import cliente
from django.contrib.auth.decorators import login_required

#user = request.user
#
#user.get_all_permissions() todos los permisos
#user.get_group_permissions() permisos de grupos
#user.has_perm()
#user.has_perms()
#user.user_permissions.remove()
#user.is_member()
#.......................

@login_required
def saludar(request):
    return HttpResponse('Hola clase')
@login_required
def crear(request):
    formulario = FormularioCliente(request.POST)
    titulo = 'Creacion de datos'
    if request.method ==  'POST':
        if formulario.is_valid():
            client = cliente()
            datos = formulario.cleaned_data
            client.cedula = datos.get('cedula')
            client.nombres = datos.get('nombres')
            client.apellidos= datos.get('apellidos')
            client.genero = datos.get('genero')
            client.save()
            return redirect('principal')
    return render(request,'crear_cliente.html',locals())

@login_required
def principal(request):
    usuario = request.user
    if usuario.has_perm('modelo.add_cliente'):
        listaClientes= cliente.objects.all().filter(estado=True).order_by('apellidos')
        context = {
            'lista':listaClientes
        }
        return render(request,'principal_cliente.html',context)
        
    else:
        return render(request,'login/acceso_prohibido.html')
    

@login_required
def modificar(request):
    dni = request.GET['cedula']
    client = cliente.objects.get(cedula = dni)
    if request.method == 'POST':
        formulario = FormularioCliente(request.POST)
        if formulario.is_valid():
            datos = formulario.cleaned_data
            client.cedula = datos.get('cedula')
            client.nombres = datos.get('nombres')
            client.apellidos= datos.get('apellidos')
            client.genero = datos.get('genero')
            client.save()
            return redirect(principal)
    else:
        formulario = FormularioCliente(instance = client)
        context = {
            'formulario':formulario
        }
    return render(request,'crear_cliente.html',context)

@login_required
def prueba(request):
    return render(request,'master.html')

@login_required
def eliminar(request):
    dni = request.GET['cedula']
    client = cliente.objects.get(cedula = dni)
    if request.method == 'POST':
            client.estado = False
            client.save()
            return redirect('principal')
    return render (request,'eliminar_cliente.html')

@login_required
def eliminarDeBase(request):
    dni = request.GET['cedula']
    client = cliente.objects.get(cedula = dni)
    if request.method == 'POST':
            client.delete()
    return render (request,'eliminar_cliente.html')
