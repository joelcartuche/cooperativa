from django.urls import path

from . import views

urlpatterns = [
    path('',views.principal,name='principal'),
    path('primerMensaje',views.saludar),
    path('crear',views.crear,name='crear'),
    path('modificar',views.modificar),
    path('pruebas',views.prueba),
    path('eliminar',views.eliminar)
]
