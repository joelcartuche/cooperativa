from django import forms
from app.modelo.models import cliente

class FormularioCliente (forms.ModelForm):
    class Meta:
        model = cliente
        fields= ["cedula","nombres","apellidos","genero"]
