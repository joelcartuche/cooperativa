from django.contrib import admin

from app.modelo.models import cliente,cuenta,transaccion,banco

class AdminCliente(admin.ModelAdmin):
    list_display = ["cedula","apellidos","nombres","genero"]
    list_editable = ["apellidos","nombres"]
    list_filter=["apellidos"]
    search_fields = ["cedula"]

    class Meta:
        model= cliente

admin.site.register(cliente,AdminCliente)

class AdminBanco(admin.ModelAdmin):
    list_display_links = None
    list_display = ["cedula","nombres","apellidos","numCuenta","cuenta"]
    list_editable = ["nombres"]
    list_filter=["nombres"]
    search_fields = ["nombres"]

    class Meta :
        model= banco

admin.site.register(banco,AdminBanco)




class AdminCuenta(admin.ModelAdmin):
    list_display_links = None
    list_display = ["nombre","saldo","tipo_de_cuenta"]
    list_editable = ["nombre","saldo"]
    list_filter=["tipo_de_cuenta"]
    search_fields = ["nombre"]

    class Meta :
        model= cuenta

admin.site.register(cuenta,AdminCuenta)

class AdminTransaccion(admin.ModelAdmin):
    list_display_links = None
    list_display = ["monto","fecha"]
    list_editable = ["monto"]


    class Meta :
        model= transaccion

admin.site.register(transaccion,AdminTransaccion)
