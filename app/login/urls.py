from django.urls import path
from . import views

urlpatterns = [
    path('',views.loginPage,name='autneticar'),
    path('logout',views.salir,name='logout')
]